// JSON to DOT converter
var fs = require('fs');
var obj;
var inputfile = "7.json";

process.argv.forEach(function (val, index, array) {
  if (index === 2) inputfile = val;
});

function stripMAC(str) {
    return str.replace('c4:93:00', '');  
}

fs.readFile(inputfile, 'utf8', function (err, data) {
	if (err) throw err;
	obj = JSON.parse(data);
	var wstream = fs.createWriteStream('Output.dot');
	wstream.write('strict digraph G {\n');
	for (m in obj) {
		var rec = obj[m];
		header=rec[0];
		if (header &&'content' in header)
			if (header['content'] === 'meshlog') {
				body=rec[1];
				if ('node_id' in header) node_id = header['node_id'];
				for (idx in body) {
					station=body[idx];
					if ('Station' in station)
						wstream.write("\"" + stripMAC(node_id) +
						"\" -> \"" + stripMAC(station['Station']) + "\"\n");
				}
			}
	}
	
	wstream.write("}");
	wstream.end();
});
