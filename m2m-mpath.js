//m2m-socket-sub.js
//Alex Pressl for imitrix GmbH 2015

var scClient = require('socketcluster-client');
var options = {
 protocol: 'http',
 hostname: 'acc.ledwifi.de',
 port: 8051,
 autoReconnect: true
};

var socket = scClient.connect(options);

var p   = [];
p.push(socket.subscribe('mpath'));

console.log("[ []");

for (channel in p) {
 p[channel].watch(function (data) {
  console.log(",");
  console.log(JSON.stringify(data,null,2));
 });
}

process.on('SIGINT', function() {
    console.log("]");

    process.exit();
});
