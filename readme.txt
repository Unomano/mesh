Neighboring nodes:
==================

1) Collect data:
node m2m.js > 1.json

2) Convert mesh stations info to DOT file "Output.dot"
node parser.js 1.json

3) Open Output.dot with Graphviz (gvedit.exe).
It will create mesh nodes graph like Output.png


Mesh path:
==========

1) Collect data:
node m2m-mpath.js > mpath.json

2) Convert mesh stations info to DOT file "Path.dot"
node mparser.js mpath.json

3) Run ./draw.sh
It will create mesh path graph like Path.png 


Graphviz software:

http://www.graphviz.org/Download..php