// JSON to DOT converter
var fs = require('fs');
var obj;
var inputfile = "m1.json";

process.argv.forEach(function (val, index, array) {
  if (index === 2) inputfile = val;
});

var regex = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;

function stripMAC(str) {
    return str.replace('c4:93:00', '');  
}

fs.readFile(inputfile, 'utf8', function (err, data) {
	if (err) throw err;
	obj = JSON.parse(data);
	var wstream = fs.createWriteStream('Path.dot');
	wstream.write('digraph G {\n');
	for (m in obj) {
		var rec = obj[m];
		header=rec[0];
		if (header &&'content' in header)
			if (header['content'] === 'mpath') {
				body=rec[1];
				if ('node_id' in header) node_id = header['node_id'];
				for (idx in body) {
					path=body[idx];
					var res = path.split(" ");
					if (regex.test(res[0]))
						wstream.write("\"" + stripMAC(node_id) + "\" -> \"" + stripMAC(res[1]) + 
						"\" [label=\"to" + stripMAC(res[0]) + "\"]\n");
				}
			}
	}
	
	wstream.write("}");
	wstream.end();
});
